 //sample comments - can only make comments in single line

 /*multi line
     comments in JS, is much like CSS and HTML, is not read by the browser
     So, these comments are often used to add notes and to add markers to our code
//  */
// console.log('I am external');

// let num=10;
// console.log(num)

// myVariable = 'New Initialized Value'
// console.log(myVariable)

// myVariable2 = 'Initialized Value'
// let myVariable2
// console.log(myVariable2)

// let num2 = 5;
// console.log(num2);

// num2 = 5 + 3;
// console.log(num2);

// const num1 = 6;
// console.log(num1);

// //Declaring multiple variables
// let brand = 'Toyota' , model = 'Vios' , type = 'Sedan';
// console.log(brand)
// console.log(model)
// console.log(type)

// let country = 'Philippines;   
//    let capital = 'Manila';
//    console.log(country);
//    console.log(capital);

let firstName = 'Gyver';
let lastName = 'Aquino';
let fullName = firstName  + "" + lastName;

console.log(fullName);

let word1 = 'is';
let word2 = 'bootcamper';
let word3 = 'of';
let word4 = 'School';
let word5 = 'Zuitt Coding';
let word6 = 'a';
let word7 = 'Institute';
let word8 = 'Bootcamp';
let space = ' ';


let sentence = firstName + space + lastName + space + word1 + space + word6 + space + word2 + space + word3 + space + word5 + space + word8 + space + word7 + space + word4;
console.log(sentence);
// Template literals (``) allows us to create a string with the use of backticks.
// ${placeholder}

let sentenceLiteral = `${fullName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word8} ${word7}`;
console.log(sentenceLiteral);

// number data type
let numString1 = '7';
let numString2 = '6';  
let numA = 7;
let numB = 5;
let num3 = 5.5;
let num4 = .5;

console.log(numString1 + numString2);
console.log(numA + numB);
console.log(numA + num3);
console.log(num3 + num4);


// forced coertion - When one data type is forced to change to complete an operation

// string + num = concatenation
console.log(numString1 + numA); 
// above ecaple ressulted in concatenation
console.log(num3 + numString2);

// parseInt() - this can change the type of numeric string to a proper number
console.log(num4 + parseInt(numString1));
// numString1 was paresd into a proper number

let sum = numA + parseInt(numString1);
console.log(sum);
// numString1 was parsed into a proper number

// Mathematical operations (+,-,*,/,%)
// subtraction
   
console.log(num3 - numA);
// -1.5 results in proper mathematical operation

console.log(num3 - num4);

console.log(numString1 - numB);
// 2 results in proper mathematical operation because the string was forced to becomae a number

console.log(numString2 - numB);

console.log(numString1 - numString2);
// In subtraction, numeric string will not concatenate and instead forcibly change its

let sample = 'Gyver';
console.log(sample - numString1);
// Nan - result in not number. When trying to perform subtraction between alphanumeric string and numeric string. The result is NaN

// Multiplication
console.log(numA * numB);

console.log(numString1 * numA);

console.log(numString1 * numString2);

let product = numA * numB;
let product2 = numString1 * numA;
let product3 = numString1 * numString2;

// Division
console.log(product/numB);

console.log(product2/5);

console.log(numString2/numString1);

// Multiplication/Division by 0
console.log(product * 0);

console.log(product3 /0);
// dividion by 0 is not accurately and should not be done -INFINITY

// % Modulo - remainder of division operation
console.log(product2 % numB);
// product2/numB = remainder = 4

console.log(product3);
console.log(product2);
console.log(product2 % product3);

// Boolean (true or false)
// boolean is usually used for logic operations
// or if-else conditions

// When creating a variable which will contain a boolean, the variable name is usually a yes or no question

let isAdmin = true;
let isMarried =  false;
let isMVP =  true;

// you can concatenate string and boolean
console.log('Does Gyver is married?' +  isMarried);


let array = ['Goku', 'Piccolo', 'Gohan', 'Vegeta'];
console.log(array);


let hero = {
    heroName: 'One Punch Man',
    isActive: true,
    salary: 500,
    realName: 'Saitama',
    height: 200,
    }

console.log(hero);


let band = ['Cueshe', 'Rivermaya', 'Callalily', '6Cyclemind'];
console.log(band);


let person = {
    firstName: 'Gyver',
    lastName: 'Aquino',
    isDeveloper: true,
    hasPortfolio: true,
    age: 26,
}

console.log(person);

let grades = {
    quiz1: 90,
    quiz2: 95,
    quiz3: 100

}

grades = [87, 88, 90];

let sampleNull = null;

let sampleUndefined;

console.log(sampleNull);
console.log(sampleUndefined);

let foundResult = null;
let person2 = {
    name: 'Juan',
    isAdmin: true,
    age: 35
}

console.log(person2.isAdmin);


// function
 function greet(){
 console.log('Hello, my name is Saitama.');
 }
 greet();

 // Parameter and Arguments
 // Parameter acts a name variable/container that exists ONLY inside of the function. This is used as to store information to act as a stand-in or the container the value passed into the function as argument

 function printNAme (name){
    console.log(`My name is ${name}`)
 }

 printNAme('thonie');
 // when function is onvoked and data is passed, we call the data as argument.
 //  In this invocation, 'thonie' is an argument passed into our printName function and is represented by the "name" parameter within our function

 function displayNum(number){
    console.log(number);

 }

 displayNum(5000);

 // Multiple Parameters and Arguments

 function displayFullName(firstName, lastName, age){
    console.log(`${firstName}, ${lastName} ${age}`);
 }

displayFullName('Juan','Masipag', 500);

// return keyword
function createName(firstName, lastName){
    return `${firstName} ${lastName}`
    console.log("I will no longer run because the function's value has been return")
}







